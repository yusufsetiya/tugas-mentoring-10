<?php $this->load->view('template/template_scripts') ?>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1><?php echo isset($nama_menu) ? $nama_menu : ''; ?></h1>

    </div>
  </section>
  <div class="section-body mt-3">
    <div class="card">
      <?php echo $output->output; ?>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="detailModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title fs-5" id="exampleModalLabel">Detail Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="content">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script>
  function showModal($param) {
    var id = id;
    $.ajax({
      type: "GET",
      url: "<?php echo base_url('index.php/produk/index/getModal/') ?>" + $param,
      success: function(response) {
        // console.log(response)
        $('#content').html(response);
        $('#detailModal').modal('show');
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
      }
    })
  }
</script>
<?php
if (isset($output->js_files)) {
  foreach ($output->js_files as $file) {
    echo '<script src="' . $file . '"></script>';
  }
}

?>