<div class="modal fade" tabindex="-1" id="modal-highlight">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="form-highlight" enctype="form-data/multipart">
                <div id="id_highlight"></div>
				<div class="modal-header bg-warning">
					<b class="modal-title text-white">Tambah highlight</b>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
                    <div class="form-group">
                      <label for="inputEmail3">Nama highlight Kelas Program</label>
                      <input type="text" name="nama_highlight" id="nama_highlight" class="form-control" required="required">
                    </div>
                </div>
				<div class="modal-footer">
					<button class="btn btn-warning" data-dismiss="modal"><i class="fas fa-ban"></i> Tutup</button>
					<button class="btn btn-success" type="submit"><i class="fas fa-save"></i> Simpan</button>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
        $("#form-highlight").validate({
            rules: {
                nama_highlight: {
                    required: !0
                },
            },
            messages:{
                nama_highlight: {
                    required: 'Form nama highlight tidak boleh kosong'
                },
            },
            submitHandler: function (e) {
                var url;
                if (aksi == 'simpan') {
                    url = site_url + "/highlight/index/simpan";
                } else {
                    url = site_url + "/highlight/index/update";
                }

                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    async : false,
                    data: new FormData($('#form-highlight')[0]),
                    success: function (data) {
                        if (data.success) {
                            $('#modal-highlight').modal('hide');
                            $('#form-highlight')[0].reset();
                            oTable.draw();
                            iziToast.success({
                                title: 'Proses Berhasil !',
                                message: data.pesan,
                                position : 'topRight',
                                timeout : 10000
                            });
                        } else {
                            iziToast.error({
                                title: 'Proses Gagal !',
                                message: data.pesan,
                                position : 'topRight',
                                timeout : 10000
                            });
                        }
                    },
                    error: function(XMLHttpRequest) {
                        console.log(XMLHttpRequest);
                    }
                });
            }
        });

	   //  $('#form-highlight').on('submit', function(e) {
    //      e.preventDefault();
    //      $.ajax({
    //         url: $('#form-highlight').attr('action'),
    //         dataType: 'json',
    //         type: 'POST',
    //         contentType: false,
    //         processData: false,
    //         async : false,
    //         data: new FormData($('#form-highlight')[0]),
    //         success: function (data) {
    //             if (data.success) {
    //                 $('#modal-highlight').modal('hide');
    //                 $('#form-highlight')[0].reset();
    //                 $('[name=keterangan]').summernote('code','');
    //                 oTable.draw();
    //                     iziToast.success({
    //                         title: 'Proses Berhasil !',
    //                         message: data.pesan,
    //                         position : 'topRight',
    //                         timeout : 10000
    //                     });
    //             } else {
    //                 iziToast.error({
    //                     title: 'Proses Gagal !',
    //                     message: data.pesan,
    //                     position : 'topRight',
    //                     timeout : 10000
    //                 });
    //             }
    //         },
    //         error: function(XMLHttpRequest) {
    //             console.log(XMLHttpRequest);
    //         }
    //     });
    // })
})
</script>