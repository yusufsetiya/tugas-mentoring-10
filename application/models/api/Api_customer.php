<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Api_customer extends CI_Model {

    function __construct() {
        parent::__construct();
        require APPPATH.'libraries/phpmailer/src/Exception.php';
        require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH.'libraries/phpmailer/src/SMTP.php';
        date_default_timezone_set('Asia/Jakarta');
    }

    function registrasi_customer($data) {

        $cek_user = $this->db->query("SELECT *
                                    FROM user
                                    WHERE ((username IS NOT NULL AND username = ?) OR (email IS NOT NULL AND email = ?) OR (kontak_name IS NOT NULL AND kontak_name = ?))
                                    AND is_active = 1", array($data['kontak_name'], $data['email'],$data['kontak_name']));

        if ($cek_user->num_rows()===0) {

            $kode_pendaftaran = mt_rand(100000, 999999);

            $kirim['pin_pendaftaran']= $kode_pendaftaran;
            $kirim['nomor_hp']= $data['kontak_name'];

            $cek_nomor = send_pin($kirim);
            if ($cek_nomor['status']=='0') {
                return ['status'=>"failed","message"=>"Nomor anda tidak terdaftar","data"=>""];
            }else {

                $this->db->trans_begin();

               $date = date("Y-m-d H:i:s");
                $currentDate = strtotime($date);
                $futureDate = $currentDate+(60*30);
                $expired = date("Y-m-d H:i:s", $futureDate);

                $prefix = "RSN";
                $kode_customer = auto_code($prefix . date('ymd'), "");
                $users['pin_pendaftaran'] = $kode_pendaftaran;
                $users['email'] = $data['email'];
                $users['username'] = $data['kontak_name'];
                $users['real_name'] = $data['real_name'];
                $users['kontak_name'] = $data['kontak_name'];
                $users['password'] = hash('sha256', $data['password']);
                $in_user = $this->db->query("INSERT INTO user(pin_pendaftaran,email, username,real_name, kontak_name, password) values(?,?,?,?,?,?)", $users);
                $id_user = $this->db->insert_id();

                $in_group = $this->db->query("INSERT INTO user_group_combo(id_user, id_group) values(?,?)", array('id_user'=>$id_user,'id_group'=>'9'));

                $customer['id_customer'] = getUUID();
                $customer['kode_customer'] = $kode_customer;
                $customer['nama'] = $data['real_name'];
                $customer['nomor_hp'] = $data['kontak_name'];
                $customer['email'] = $data['email'];
                $customer['fk_id_user'] = $id_user;
                $customer['created_by'] = $id_user;
                $customer['pin_pendaftaran'] = $kode_pendaftaran;
                $customer['pin_expired'] = $expired;
                $in_customer = $this->db->query("INSERT INTO customer(id_customer,kode_customer, nama, nomor_hp, email, fk_id_user, created_by, pin_pendaftaran, pin_expired) values(?,?,?,?,?,?,?,?,?)", $customer);

               $data_response['id_customer'] = $customer['id_customer'];
               $data_response['id_user'] = $id_user;
               $data_response['pin_pendaftaran'] = $kode_pendaftaran;
               $data_response['kode_customer'] = $kode_customer;

                if ($this->db->trans_status()===false) {
                    $this->db->trans_rollback();
                    return ['status'=>'failed','message'=>'Pendaftaran gagal, terjadi kesalahan sistem dalam pemrosesan data','data'=>''];
                }else {
                    $this->db->trans_commit();
                        return ['status'=>'ok','message'=>'Pendaftaran anda berhasil dilakukan. Langkah selanjutkan, lakukan pengecekan PIN Pendaftaran yang dikirim melalui nomor WhatsApp anda','data'=>$data_response];
                }
                $this->db->trans_complete();
            }

        }else {
            return ['status'=>'Failed','message'=>'Username/nomor HP/Email anda telah terdaftar harap cek ulang data anda','data'=>''];
        }

    }

    function _cekUser($params){
        $username = $this->db->query("SELECT * FROM user where((username IS NOT NULL and username=?)) AND is_active=1", $params['username']);
        if ($username->num_rows()==0) {
            return ['status'=>'failed','message'=>'Username sudah terdaftar, harap ganti username anda'];
        }

        $email = $this->db->query("SELECT * FROM user where((email IS NOT NULL and email=?)) AND is_active=1",$params['email']);
        if ($email->num_rows()==0) {
            return ['status'=>'failed','message'=>'Email sudah terdaftar, harap ganti email anda'];
        }

        $nomor_hp = $this->db->query("SELECT * FROM user where((kontak_name IS NOT NULL and kontak_name=?)) AND is_active=1",$params['kontak_name']);
        if ($nomor_hp->num_rows()==0) {
            return ['status'=>'failed','message'=>'Nomor HP sudah terdaftar, harap ganti nomor HP anda'];
        }

        $hp_customer = $this->db->query("SELECT * FROM customer where((nomor_hp IS NOT NULL AND nomor_hp=?))",$params['kontak_name']);
        if ($hp_customer->num_rows()==0) {
            return ['status'=>'failed','message'=>'Nomor HP sudah terdaftar, harap ganti nomor HP anda'];
        }

    }

    function verifikasi($data)
    {
        $sql = $this->db->query( "SELECT customer.*, ref_kecamatan.nama_kecamatan, ref_kab_kota.nama_kab_kota, ref_provinsi.nama_propinsi from customer left join ref_kecamatan on ref_kecamatan.kode_kecamatan=customer.id_kecamatan left join ref_kab_kota on ref_kab_kota.kode_kab_kota=ref_kecamatan.kode_kab_kota left join ref_provinsi on ref_provinsi.kode_propinsi=ref_kab_kota.kode_provinsi where customer.nomor_hp=? AND customer.pin_pendaftaran=?", array($data['nomor_hp'], $data['pin_pendaftaran']));
        if ($sql->num_rows()==0) {
            return ['status'=>'failed','message'=>'data tidak ditemukan.','data'=>'0'];

        }else {
            $cs = $sql->row();
            if ($cs->pin_validasi!=NULL || $cs->pin_validasi !='') {

                return [
                    'status'=>'failed',
                    'message'=>'Verifikasi sudah dilakukan',
                    'data'=>''];

            }else {

                $tanggal_kirim = date('Y-m-d H:i:s');

                if ($tanggal_kirim>$cs->pin_expired) {
                    return ['status'=>'failed','message'=>'PIN anda sudah kadaluarsa','data'=>''];
                } else {

                    $this->db->trans_begin();
                    $this->db->where("kontak_name", $data['nomor_hp']);
                    $this->db->update('user', array('is_active'=>'1'));

                    $this->db->where("kode_customer", $cs->kode_customer);
                    $this->db->update("customer", array('pin_validasi'=>$data['pin_pendaftaran']));

                    if ($data['kode_referal']!="" || $data['kode_referal']!= NULL) {
                        $cek_referal = $this->db->query("SELECT * FROM customer where kode_customer=?", array($data['kode_referal']));

                        if ($cek_referal->num_rows()>0) {
                            $ref = $cek_referal->row();

                            $total_saldo = $ref->saldo+1000;

                            $this->db->where("kode_customer", $data['kode_referal']);
                            $this->db->update("customer", array('saldo'=>$total_saldo));
                            $this->db->where("kode_customer", $cs->kode_customer);
                            $this->db->update("customer", array("from_kode_customer"=>$data['kode_referal']));

                            $trx_referal['id_referensi'] = getUUID();
                            $trx_referal['from_id_customer'] = $cs->kode_customer;
                            $trx_referal['to_id_customer'] = $data['kode_referal'];
                            $trx_referal['created_by'] = $cs->kode_customer;
                            $this->db->insert("trx_referal", $trx_referal);
                        }
                    }

                    if ($this->db->trans_status()===false) {
                        $this->db->trans_rollback();
                        return ['status'=>'failed','message'=>'Proses validasi PIN Pendaftaran gagal dilakukan','data'=>''];
                    }else {
                        $this->db->trans_commit();
                        $params['username'] = $data['username'];
                        $params['password'] = $data['password'];
                        $params['nomor_hp'] = $data['nomor_hp'];

                        $params['judul'] = 'Informasi username & Password Customer';
                        $params['subjek'] = 'Penting !';
                        $params['email_tujuan'] = $data['email'];
                        $params['link'] = LINK_FRONTEND.'login/index';
                        $params['link_backend'] = site_url();
                        $params['body_email'] = $this->load->view("template_verifikasi", $params, true);
                        $this->kirimVerifikasi($params);

                        $session_login['id_customer'] = $cs->id_customer;
                        $session_login['id_user'] = $cs->fk_id_user;
                        $session_login['username'] = $data['username'];
                        $session_login['real_name'] = $cs->nama;
                        $session_login['id_role'] = '9';
                        $session_login['alamat'] = $cs->alamat;
                        $session_login['type'] = '';
                        $session_login['nik'] = $cs->nik;
                        $session_login['tempat_lahir'] = $cs->tempat_lahir;
                        $session_login['tanggal_lahir'] = $cs->tanggal_lahir;
                        $session_login['jenis_kelamin'] = $cs->jenis_kelamin;
                        $session_login['status'] = $cs->status;
                        $session_login['saldo'] = $cs->saldo;
                        $session_login['pendidikan'] = $cs->pendidikan;
                        $session_login['pekerjaan'] = $cs->pekerjaan;
                        $session_login['penghasilan'] = $cs->penghasilan;
                        $session_login['id_provinsi'] = $cs->id_provinsi;
                        $session_login['id_kabupaten'] = $cs->id_kabupaten;
                        $session_login['id_kecamatan'] = $cs->id_kecamatan;
                        $session_login['nama_propinsi'] = trim($cs->nama_propinsi);
                        $session_login['nama_kab_kota'] = trim($cs->nama_kab_kota);
                        $session_login['nama_kecamatan'] = trim($cs->nama_kecamatan);
                        $session_login['url_ktp'] = $cs->url_ktp;
                        $session_login['url_foto'] = $cs->url_foto;
                        $session_login['is_tab_umrah'] = '0';
                        $session_login['is_umrah'] = '0';

                        return ['status'=>'success','message'=>'Verifikasi PIN Pendaftaran anda berhasil dilakukan. gunakan username/email yang telah dikirim melalui email anda untuk menikmati layanan Rosana Tour & Travel','data'=>$session_login];
                    }

                }

            }


        }
    }

    public function resetHP($data)
    {
        $get = $this->db->query("SELECT email, nomor_hp from customer  where email=? and pin_validasi is null", array($data['email']));
        if ($get->num_rows()>0) {
            $res = $get->row();
            $result['email'] = $res->email;
            $result['nomor_hp'] = $res->nomor_hp;
            return ['status'=>'success','message'=>'Masukan nomor HP baru','data'=>$result];
        }else {
            return ['status'=>'failed','message'=>'Akun tidak ditemukan atau sudah aktif','data'=>''];
        }
    }

    public function ubah_nomor($data)
    {
        $get = $this->db->query("SELECT * FROM user where((kontak_name IS NOT NULL AND kontak_name=?)) AND ((email IS NOT NULL AND email=?)) AND is_active=1", array($data['nomor_hp'], $data['email']));

        if ($get->num_rows()>0) {
            return [
                'status'=>'failed',
                'message'=>'nomor HP sudah terdaftar di sistem',
                'data'=>$get->num_rows()];
        }else {

            $get_cs = $this->db->query("SELECT * FROM customer where((nomor_hp IS NOT NULL AND nomor_hp=?)) AND ((email IS NOT NULL AND email=?))",array($data['nomor_hp'], $data['email']));
            if ($get_cs->num_rows()>0) {
               return [
                'status'=>'failed',
                'message'=>'nomor HP sudah terdaftar di sistem',
                'data'=>$get_cs->num_rows()];

            }else {
                $this->db->trans_begin();

                $update_pin = mt_rand(100000, 999999);
                $date = date("Y-m-d H:i:s");
                $currentDate = strtotime($date);
                $futureDate = $currentDate+(60*30);
                $expired = date("Y-m-d H:i:s", $futureDate);
                $pin_validasi = NULL;

                $this->db->where("email", $data['email']);
                $this->db->update("user", array('kontak_name'=>$data['nomor_hp'],'username'=>$data['nomor_hp'],'is_active'=>'0'));

                $customer['pin_pendaftaran'] = $update_pin;
                $customer['pin_expired'] = $expired;
                $customer['pin_validasi'] = NULL;
                $customer['nomor_hp'] = $data['nomor_hp'];
                $this->db->where("email", $data['email']);
                $this->db->update("customer", $customer);
                if ($this->db->trans_status()===false) {
                    $this->db->trans_rollback();
                    return [
                        'status'=>'failed',
                        'message'=>'Update PIN gagal dilakukan',
                        'data'=>'0'];
                }else {
                    $kirim['pin_pendaftaran'] = $update_pin;
                    $kirim['nomor_hp'] = $data['nomor_hp'];

                    $response_wa = send_pin_forgot($kirim);
                    if ($response_wa['status']=='0') {
                        $this->db->trans_rollback();
                        return [
                            'status'=>'failed',
                            'message'=>'Nomor HP tidak terdaftar dalam aplikasi, harap cek kembali nomor anda',
                            'data'=>'0'];
                    }else {
                        $this->db->trans_commit();
                        return [
                            'status'=>'success',
                            'message'=>'Proses perubahan nomor HP berhasil.Silakan cek pesan yang dikirimkan pada nomor HP anda untuk melakukan verifikasi ulang',
                            'data'=>'1'];
                    }
                    $this->db->trans_complete();
                }
            }
        }
    }

    public function verifikasiPerubahanHP($data)
    {
        $get = $this->db->query("SELECT customer.*, user.username, ref_kecamatan.nama_kecamatan, ref_kab_kota.nama_kab_kota, ref_provinsi.nama_propinsi from customer
            left join user on user.id_user=customer.fk_id_user
            left join ref_kecamatan on ref_kecamatan.kode_kecamatan=customer.id_kecamatan
            left join ref_kab_kota on ref_kab_kota.kode_kab_kota=ref_kecamatan.kode_kab_kota
            left join ref_provinsi on ref_provinsi.kode_propinsi=ref_kab_kota.kode_provinsi
            where customer.nomor_hp=? AND customer.pin_pendaftaran=?",array($data['nomor_hp'],$data['pin_pendaftaran']) );

        if ($get->num_rows()>0) {
            $dt_customer = $get->row();
            $tanggal_kirim = date('Y-m-d H:i:s');

            if ($tanggal_kirim>$dt_customer->pin_expired) {
                return [
                    'status'=>'failed',
                    'message'=>'PIN yang anda masukkan sudah kadaluarsa',
                    'data'=>''];
            }else {
                $this->db->trans_begin();
                $this->db->where("kode_customer", $dt_customer->kode_customer);
                $this->db->update("customer", array('pin_validasi'=>$data['pin_pendaftaran']));

                $this->db->where("kontak_name", $data['nomor_hp']);
                $this->db->update("user", array('is_active'=>'1'));

                if ($this->db->trans_status()===false) {
                    $this->db->trans_rollback();
                    return [
                        'status'=>'failed',
                        'message'=>'Verifikasi PIN anda gagal dilakukan. Silakan cek ulang PIN anda atau tanyakan kepada customer service apabila menemui kesulitan',
                        'data'=>''];
                }else {
                    $this->db->trans_commit();
                    $session_login['id_customer'] = $dt_customer->id_customer;
                    $session_login['id_user'] = $dt_customer->fk_id_user;
                    $session_login['real_name'] = $dt_customer->nama;
                    $session_login['username'] = $dt_customer->username;
                    $session_login['email'] = $dt_customer->email;
                    $session_login['nomor_hp'] = $data['nomor_hp'];
                    $session_login['id_role'] = '9';
                    $session_login['alamat'] = $dt_customer->alamat;
                    $session_login['type'] = '';
                    $session_login['nik'] = $dt_customer->nik;
                    $session_login['tempat_lahir'] = $dt_customer->tempat_lahir;
                    $session_login['tanggal_lahir'] = $dt_customer->tanggal_lahir;
                    $session_login['jenis_kelamin'] = $dt_customer->jenis_kelamin;
                    $session_login['status'] = $dt_customer->status;
                    $session_login['saldo'] = $dt_customer->saldo;
                    $session_login['pendidikan'] = $dt_customer->pendidikan;
                    $session_login['pekerjaan'] = $dt_customer->pekerjaan;
                    $session_login['penghasilan'] = $dt_customer->penghasilan;
                    $session_login['id_provinsi'] = $dt_customer->id_provinsi;
                    $session_login['id_kabupaten'] = $dt_customer->id_kabupaten;
                    $session_login['id_kecamatan'] = $dt_customer->id_kecamatan;
                    $session_login['nama_propinsi'] = trim($dt_customer->nama_propinsi);
                    $session_login['nama_kab_kota'] = trim($dt_customer->nama_kab_kota);
                    $session_login['nama_kecamatan'] = trim($dt_customer->nama_kecamatan);
                    $session_login['url_ktp'] = $dt_customer->url_ktp;
                    $session_login['url_foto'] = $dt_customer->url_foto;
                    $session_login['is_tab_umrah'] = '0';
                    $session_login['is_umrah'] = '0';

                    return [
                        'status'=>'success',
                        'message'=>'Verifikasi PIN anda berhasil dilakukan. gunakan username/email yang telah dikirim melalui email anda untuk menikmati layanan Rosana Tour & Travel',
                        'data'=>$session_login];
                }

            }
        }else {
            return [
                'status'=>'failed',
                'message'=>'Verifikasi PIN anda gagal dilakukan. Silakan cek ulang PIN anda atau tanyakan kepada customer service apabila menemui kesulitan',
                'data'=>''];
        }

    }

    public function lupaSandi($data)
    {
        $get = $this->db->query("SELECT id_customer,pin_pendaftaran, nomor_hp, email FROM customer where ((nomor_hp IS NOT NULL AND nomor_hp=?) OR (email IS NOT NULL AND email=?))", array($data['kontak_user'], $data['kontak_user']));
        if ($get->num_rows()>0) {
            $result = $get->row();

            $update_pin = mt_rand(100000, 999999);
            $date = date("Y-m-d H:i:s");
            $currentDate = strtotime($date);
            $futureDate = $currentDate+(60*30);
            $expired = date("Y-m-d H:i:s", $futureDate);
            $pin_validasi = NULL;

            $pin['pin_pendaftaran'] = $update_pin;
            $pin['pin_expired'] = $expired;
            $pin['pin_validasi'] = NULL;

            $this->db->where("id_customer", $result->id_customer);
            $this->db->update("customer", $pin);

            if (is_numeric($data['kontak_user'])) {
                $kirim['pin_pendaftaran'] = $update_pin;
                $kirim['nomor_hp'] = $result->nomor_hp;

                $res_wa = send_pin_forgot($kirim);
                if ($res_wa['status']=='0') {

                    return [
                        'status'=>'failed',
                        'message'=>'Nomor HP tidak terdaftar dalam aplikasi, harap cek kembali nomor anda',
                        'data'=>'0'];
                }else {
                    return [
                        'status'=>'success',
                        'message'=>'Silahkan cek nomor HP/email anda dan masukan PIN yang telah dikirimkan',
                        'data'=>$update_pin];
                }
            }else {
                $params['pin_pendaftaran'] = $update_pin;
                $params['judul'] = 'Reset kata sandi';
                $params['subjek'] = 'Penting !';
                $params['email_tujuan'] = $data['kontak_user'];
                  $params['link'] = LINK_FRONTEND.'/lupa_password/indeform_ubah';
                $params['link_backend'] = site_url();
                $params['body_email'] = $this->load->view("template_lupa_sandi", $params, true);
                $resultEmail = $this->sendForgotEmail($params);

                if ($resultEmail['status']==true) {

                    return [
                        'status'=>'success',
                        'message'=>'Silahkan cek nomor HP/email anda dan masukan PIN yang telah dikirimkan',
                        'data'=>$update_pin];
                }else {
                    return [
                        'status'=>'failed',
                        'message'=>'Email tidak valid, harap cek kembali email anda',
                        'data'=>'0'];
                }
            }

        }else {
            return [
                'status'=>'failed',
                'message'=>'Data tidak terdaftar dalam aplikasi',
                'data'=>'0'];
        }
    }

    public function updateSandi($data)
    {
        if (is_numeric($data['kontak_name'])) {
            $get = $this->db->query("UPDATE user set password=SHA2(?,256) where kontak_name=?", array($data['password'], $data['kontak_name']));
            if ($get) {

                return [
                    'status'=>'success',
                    'message'=>'Proses berhasil ! Gunakan username/nomor HP/Email serta kata sandi baru untuk login dalam aplikasi Rosana',
                    'data'=>'1'];
            }else {
                return [
                    'status'=>'failed',
                    'message'=>'Proses gagal ! Silahkan coba beberapa saat lagi untuk melakukan update kata sandi',
                    'data'=>'0'];
            }
        } else {
            $get = $this->db->query("UPDATE user set password=SHA2(?,256) where email=?", array($data['password'], $data['kontak_name']));
            if ($get) {

                return [
                    'status'=>'success',
                    'message'=>'Proses berhasil ! Gunakan username/nomor HP/Email serta kata sandi baru untuk login dalam aplikasi Rosana',
                    'data'=>'1'];
            }else {
                return [
                    'status'=>'failed',
                    'message'=>'Proses gagal ! Silahkan coba beberapa saat lagi untuk melakukan update kata sandi',
                    'data'=>'0'];
            }

        }

    }

    public function getReferal($id_user)
    {
        $result = array();
        $histori_ref = array();
        $i = 0;

        $get = $this->db->query("SELECT kode_customer, pin_pendaftaran, saldo from customer where fk_id_user=?",array($id_user));

        if ($get->num_rows()>0) {

            $rs = $get->row();

            $result['kode_customer'] = $rs->kode_customer;
            $result['pin_pendaftaran'] = $rs->pin_pendaftaran;
            $result['saldo'] = $rs->saldo;

            $histori = $this->db->query("SELECT trx_referal.*, customer.kode_customer from trx_referal left join customer on customer.id_customer=trx_referal.to_id_customer where trx_referal.to_id_customer=?", array($rs->kode_customer));
            if ($histori->num_rows()>0) {

                foreach ($histori->result() as $rows) {
                    $histori_ref[$i]['id_referensi'] = $rows->id_referensi;
                    $histori_ref[$i]['from_id_customer'] = $rows->from_id_customer;
                    $histori_ref[$i]['to_id_customer'] = $rows->to_id_customer;
                    $histori_ref[$i]['kode_customer'] = $rows->kode_customer;
                    $histori_ref[$i]['nominal_komisi'] = $rows->nominal_komisi;
                    $i++;
                }
            }else {
                $histori_ref = NULL;

            }
                $result['referal'] = $histori_ref;

                return [
                    'status'=>'success',
                    'message'=>'Data referal customer ditemukan',
                    'data'=>$result];

        }else {
            return [
                'status'=>'failed',
                'message'=>'Data referal kosong',
                'data'=>''];
        }
    }

    public function editCustomer($id)
    {
        $result = array();
        $user = array();
        $cst = array();

        $get_user = $this->db->query("SELECT * FROM user where id_user=?", array($id));
        if ($get_user->num_rows()>0) {
            $usr = $get_user->row();
            $user['id_user'] = $usr->id_user;
            $user['email'] = $usr->email;
            $user['Username'] = $usr->username;
            $user['real_name'] = $usr->real_name;
            $user['kontak_name'] = $usr->kontak_name;
            $user['is_active'] = $usr->is_active;
            $user['password'] = $usr->password;
        }else {
            $user = NULL;
        }

        $customer = $this->db->query("SELECT * from customer where fk_id_user=?", array($id));
        if ($customer->num_rows()>0) {
            $cs = $customer->row();

            $cst['id_customer'] = $cs->id_customer;
            $cst['kode_customer'] = $cs->kode_customer;
            $cst['pin_pendaftaran'] = $cs->pin_pendaftaran;
            $cst['nik'] = $cs->nik;
            $cst['nama'] = $cs->nama;
            $cst['nomor_hp'] = $cs->nomor_hp;
            $cst['nama_ibu'] = $cs->nama_ibu;
            $cst['nama_ayah'] = $cs->nama_ayah;
            $cst['tempat_lahir'] = $cs->tempat_lahir;
            $cst['tanggal_lahir'] = $cs->tanggal_lahir;
            $cst['jenis_kelamin'] = $cs->jenis_kelamin;
            $cst['status'] = $cs->status;
            $cst['email'] = $cs->email;
            $cst['pendidikan'] = $cs->pendidikan;
            $cst['pekerjaan'] = $cs->pekerjaan;
            $cst['penghasilan'] = $cs->penghasilan;
            $cst['id_provinsi'] = $cs->id_provinsi;
            $cst['id_kabupaten'] = $cs->id_kabupaten;
            $cst['id_kecamatan'] = $cs->id_kecamatan;
            $cst['saldo'] = $cs->saldo;
            $cst['alamat'] = $cs->alamat;
            $cst['url_ktp'] = $cs->url_ktp;
            $cst['url_foto'] = $cs->url_foto;
            $cst['id_cabang'] = $cs->id_cabang;
            $cst['is_tab_umrah'] = $cs->is_tab_umrah;
            $cst['is_umrah'] = $cs->is_umrah;
            $cst['fk_id_user'] = $cs->fk_id_user;
        }else {
            $cst = NULL;
        }

        $result['user'] = $user;
        $result['customer'] = $cst;

        return [
            'status'=>'success',
            'message'=>'Data found',
            'data'=>$result];
    }

    public function updateCustomer($data, $id)
    {
        $this->db->trans_begin();
        $this->db->where("fk_id_user", $id);
        $this->db->update("customer", $data);

        $this->db->where("id_user", $id);
        $this->db->update("user", array(
            "kontak_name"=>$data['nomor_hp'],
            "email"=>$data['email'],
            'real_name'=>$data['nama']));

        if ($this->db->trans_status()=== false) {

            $this->db->trans_rollback();

            return [
                'status'=>'failed',
                'message'=>'Update data profile gagal dilakukan',
                'data'=>''];
        }else {
            $this->db->trans_commit();

            $get = $this->db->query("SELECT id_customer,nik,nama, nama_ayah,customer.id_cabang,nama_cabang,tempat_lahir, tanggal_lahir, jenis_kelamin, status, nomor_hp, email,pendidikan, pekerjaan, penghasilan,id_provinsi, id_kabupaten, id_kecamatan,nama_propinsi, nama_kab_kota,nama_kecamatan, customer.alamat, url_ktp, url_foto from customer
          left join cabang on cabang.id_cabang = customer.id_cabang
          left join ref_provinsi on ref_provinsi.kode_propinsi=customer.id_provinsi
          left join ref_kab_kota ON  ref_kab_kota.kode_kab_kota=customer.id_kabupaten
          left join ref_kecamatan on ref_kecamatan.kode_kecamatan=customer.id_kecamatan where customer.fk_id_user=?", array($id));

            $rs = $get->row();

            $detail['nama_ayah'] = $rs->nama_ayah;
            $detail['nama_cabang'] = $rs->nama_cabang;
            $detail['alamat'] = $rs->alamat;
            $detail['nik'] = $rs->nik;
            $detail['id_cabang'] = $rs->id_cabang;
            $detail['id_provinsi'] = $rs->id_provinsi;
            $detail['id_kabupaten'] = $rs->id_kabupaten;
            $detail['id_kecamatan'] = $rs->id_kecamatan;
            $detail['nama_propinsi'] = trim($rs->nama_propinsi);
            $detail['nama_kab_kota'] = trim($rs->nama_kab_kota);
            $detail['nama_kecamatan'] = trim($rs->nama_kecamatan);
            $detail['url_ktp'] = $rs->url_ktp;
            $detail['url_foto'] = $rs->url_foto;

            return [
                'status'=>'success',
                'message'=>'Profile Anda Berhasil Diupdate',
                'data'=>$detail];

        }

        $this->db->trans_complete();
    }

    function check_user($username) {
        //cek username
        $get = $this->db->query("SELECT id_user FROM user WHERE ((username IS NOT NULL AND LOWER(username) = ?) OR (email IS NOT NULL AND LOWER(email) = ?))", array($username, $username));
        if ($get->num_rows() == 0) {
            return ["status" => "failed", "message" => "Refresh token gagal. User tidak ditemukan."];
        }

        //cek status
        $get = $this->db->query("SELECT id_user, is_active FROM user WHERE ((username IS NOT NULL AND LOWER(username) = ?) OR (email IS NOT NULL AND LOWER(email) = ?))", array($username, $username))->row_array();
        if ($get['is_active'] == '0') {
            return ["status" => "failed", "message" => "Refresh token gagal. User telah dinonaktifkan."];
        }

        $id_user = $get['id_user'];
        // $id_group = $get['id_group'];
        return ["status" => "ok", "data" => ['id_user' => $id_user]];
    }

    function kirimVerifikasi($params)
    {
        $response['status'] = true;
        $response['info'] = '';

        $mail = new PHPMailer();

        $mail->IsHTML(true);    // set email format to HTML
        $mail->IsSMTP();   // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl"; // prefix for secure protocol to connect to the server
        // $mail->SMTPDebug  = 2;
        $mail->Host       = "smtp.gmail.com"; // setting GMail as our SMTP server
        $mail->Port       = 465;   // SMTP port to connect to GMail
        $mail->Username   = "cyber.rosana@gmail.com";  // alamat email kamu
        $mail->Password   = "www.rosanatourtravel.com1";            // password GMail
        $mail->SetFrom("cyber.rosana@gmail.com", $params['judul']);  //Siapa yg mengirim email
        $mail->SMTPAutoTLS = false;
        $mail->WordWrap = 50;
        $mail->Priority = 1;
        $mail->AddCustomHeader("X-MSMail-Priority: High");
        $mail->Subject    = $params['subjek'];
        $mail->Body       = $params['body_email'];
        $mail->AddAddress($params['email_tujuan']);

        $response['status'] = false;
        $response['info'] = '';

        if($mail->send())
        {
            $response['status'] = true;
            $response['info'] = '';
        }
        else
        {
            $response['status'] = false;
            $response['info'] = $mail->ErrorInfo;
        }
        return $response;
    }

    function sendForgotEmail($params)
    {
        $response['status'] = true;
        $response['info'] = '';

        $mail = new PHPMailer();

        $mail->IsHTML(true);    // set email format to HTML
        $mail->IsSMTP();   // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl"; // prefix for secure protocol to connect to the server
        // $mail->SMTPDebug  = 2;
        $mail->Host       = "smtp.gmail.com"; // setting GMail as our SMTP server
        $mail->Port       = 465;   // SMTP port to connect to GMail
        $mail->Username   = "cyber.rosana@gmail.com";  // alamat email kamu
        $mail->Password   = "www.rosanatourtravel.com1";            // password GMail
        $mail->SetFrom("cyber.rosana@gmail.com", $params['judul']);  //Siapa yg mengirim email
        $mail->SMTPAutoTLS = false;
        $mail->WordWrap = 50;
        $mail->Priority = 1;
        $mail->AddCustomHeader("X-MSMail-Priority: High");
        $mail->Subject    = $params['subjek'];
        $mail->Body       = $params['body_email'];
        $mail->AddAddress($params['email_tujuan']);

        $response['status'] = false;
        $response['info'] = '';

        if($mail->send())
        {
            $response['status'] = true;
            $response['info'] = '';
        }
        else
        {
            $response['status'] = false;
            $response['info'] = $mail->ErrorInfo;
        }
        return $response;
    }


}
