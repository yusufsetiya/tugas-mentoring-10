<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_artikel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function data_artikel($limit="") {
        $this->db->select("artikel.*,artikel_kategori.nama_kategori, user.real_name");
        $this->db->from("artikel");
        $this->db->join("artikel_kategori","artikel_kategori.id_kategori=artikel.id_kategori","left");
        $this->db->join("user","user.id_user=artikel.created_by","left");
        $this->db->order_by("artikel.created_at", 'DESC');
        if ($limit !="" || $limit!=null) {
            $this->db->limit($limit);
        }
        $get = $this->db->get();

        if ($get->num_rows() == 0) {
            return ["status" => "failed", "message" => "Data tidak ditemukan."];
        }
        $i = 0;
        foreach ($get->result() as $key => $r) {
            $result[$i]['id_artikel'] = $r->id_artikel;
            $result[$i]['penulis'] = $r->real_name;
            $result[$i]['nama_kategori'] = $r->nama_kategori;
            $result[$i]['judul_artikel'] = $r->judul_artikel;
            $result[$i]['created_at'] = $r->created_at;
            if ($r->foto_artikel!='' || $r->foto_artikel!=null) {
                $result[$i]['foto_artikel'] = base_url().$r->foto_artikel;
            }else {
                $result[$i]['foto_artikel'] = base_url('assets/img/coming-soon.png');

            }
            $result[$i]['konten_artikel'] = $r->konten_artikel;
            $i++;
        }

        // serve
        return ["status" => "ok", "data" => $result];
    }

    function detail_artikel($id) {
        $this->db->select("artikel.*,artikel_kategori.nama_kategori, user.real_name");
        $this->db->from("artikel");
        $this->db->join("artikel_kategori","artikel_kategori.id_kategori=artikel.id_kategori","left");
        $this->db->join("user","user.id_user=artikel.created_by","left");
        $this->db->order_by("artikel.created_at", 'DESC');
        $this->db->where('REPLACE(artikel.id_artikel,"-","")',str_replace("-", "", $id));
        $get = $this->db->get();

        if ($get->num_rows() == 0) {
            return ["status" => "failed", "message" => "Data tidak ditemukan."];
        }
        $r = $get->row();
        $result['id_artikel'] = $r->id_artikel;
        $result['penulis'] = $r->real_name;
        $result['nama_kategori'] = $r->nama_kategori;
        $result['judul_artikel'] = $r->judul_artikel;
        $result['created_at'] = $r->created_at;
        $result['foto_artikel'] = base_url().$r->foto_artikel;
        $result['konten_artikel'] = $r->konten_artikel;

        $img = $this->db->query("select * from artikel_images where id_artikel=?", array($r->id_artikel))->result_array();
        $i=0;
        $images = array();
        foreach ($img as $rows) {
            $images[$i]['url_image'] = base_url().$rows['url_image'];
            $i++;
        }
        $result['images'] = $images;
        // serve
        return ["status" => "ok", "data" => $result,"message"=>"data artikel ditemukan"];
    }

}
