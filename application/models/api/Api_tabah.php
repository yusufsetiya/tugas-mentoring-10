<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_tabah extends CI_Model {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function getCustomer($id)
    {
        $this->db->select("*");
        $this->db->from('customer');
        $this->db->where("fk_id_user", $id);
        $result = $this->db->get();
        return $result->row();
    }

    public function getHistoriCustomer($data)
    {
        $dataTabungan = array();
        $histori = array();
        if ($data['fk_id_user']!="") {

            $customer = $this->getCustomer($data['fk_id_user']);

            $get = $this->db->query("SELECT id_tabungan,kode_reg, nomer_va,nomor_porsi, nomor_spph,target_tabungan, sisa_target_tabungan, setoran_awal,setoran_perbulan, sisa_setoran_awal, status_tabungan from haji_tabungan where id_customer=?", array($customer->id_customer));

            if ($get->num_rows()>0) {

                $row = $get->row();

                $dataTabungan['id_tabungan'] = $row->id_tabungan;
                $dataTabungan['kode_reg'] = $row->kode_reg;
                $dataTabungan['nomer_va'] = $row->nomer_va;
                $dataTabungan['nomor_porsi'] = $row->nomor_porsi;
                $dataTabungan['nomor_spph'] = $row->nomor_spph;
                $dataTabungan['setoran_awal'] = $row->setoran_awal;
                $dataTabungan['sisa_setoran_awal'] = $row->sisa_setoran_awal;
                $dataTabungan['setoran_perbulan'] = $row->setoran_perbulan;
                $dataTabungan['target_tabungan'] = $row->target_tabungan;
                $dataTabungan['sisa_target_tabungan'] = $row->sisa_target_tabungan;
                $dataTabungan['status_tabungan'] = $row->status_tabungan;
                $dataTabungan['nama']  = $customer->nama;

                  $getSetoranAwal = $this->db->query("SELECT ht.id_setoran_awal AS id_trx, ht.tanggal_pembayaran, ht.nominal, ht.catatan, 'setoran' AS jenis FROM ht_setoran_awal ht
                        WHERE REPLACE(ht.id_tabungan_haji,'-','')=?
                        UNION
                        SELECT hs.id_setoran_tabungan AS id_trx, hs.tanggal_pembayaran, hs.nominal, hs.catatan, 'tabungan' AS jenis  FROM ht_setoran_tabungan hs
                        WHERE REPLACE(hs.id_tabungan_haji,'-','')=?", array(str_replace("-", "", $row->id_tabungan), str_replace("-", "", $row->id_tabungan)));
                  if ($getSetoranAwal->num_rows()>0) {

                      $i=0;
                      foreach ($getSetoranAwal->result() as $rows) {
                          $histori[$i]['tanggal_pembayaran'] = $rows->tanggal_pembayaran;
                          $histori[$i]['nominal'] = $rows->nominal;
                          $histori[$i]['catatan'] = $rows->catatan;
                          $histori[$i]['jenis'] = $rows->jenis;
                          $i++;
                      }
                  }else {
                      $histori = array();
                  }
                  $dataTabungan['history'] = $histori;


                  return [
                    'status'=>'success',
                    'message'=>'Data tabungan umroh berhasil ditemukan',
                    'data'=>$dataTabungan];

            }else {
                return [
                    'status'=>'failed',
                    'message'=>'Data tabungan umrah tidak ditemukan',
                    'data'=>''];
            }
        }else {
            return [
                'status'=>'failed',
                'message'=>'Data parameter tidak boleh kosong',
                'data'=>''];
        }
    }








}
