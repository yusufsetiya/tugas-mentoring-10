<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{

    protected $template = "app";
    protected $module = "product";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_produk');
    }

    public function index()
    {
        $this->data['nama_menu'] = 'Data Produk';
        $this->data['condition'] = '<div class="breadcrumb-item active">Produk</div>';
        $crud = new Grid('default');
        $model = new GroceryCrud\Core\Model($crud->getDatabaseConfig());
        $model->_enableCountRelationFilterOnInit = TRUE;
        $crud->setSkin('bootstrap-v4');
        $crud->setModel($model);
        $crud->setTable('product');
        $crud->setRelation('category_id', 'categories', 'name');

        $crud->columns(['name', 'stock', 'is_active', 'category_id', 'color', 'price']);
        $crud->addFields(['name', 'stock', 'is_active', 'category_id', 'color', 'price']);
        $crud->editFields(['name', 'stock', 'is_active', 'category_id', 'color', 'price']);
        $crud->requiredFields(['name', 'stock', 'is_active', 'category_id', 'color', 'price']);
        $crud->defaultOrdering('created_at', 'desc');
        $display = [
            'name' => 'Nama Produk',
            'slug' => 'Slug',
            'stock' => 'Stock',
            'is_active' => 'Status',
            'category_id' => 'Kategori Produk',
            'color' => 'Warna',
            'price' => 'Harga',
        ];

        $status = array(
            '0' => 'Tersedia',
            '1' => 'Kosong',
        );
        $crud->fieldType('is_active', 'dropdown', $status);
        $crud->callbackBeforeInsert(function ($stateParameters) {
            $text = preg_replace('/[^a-zA-Z0-9]+/', '.', $stateParameters->data['name']);

            // Mengonversi slug menjadi huruf kecil
            $text = strtolower($text);
            $stateParameters->data['slug'] = $text;
            $stateParameters->data['id'] = generateUUID();
            $stateParameters->data['created_at'] = date('Y-m-d H:i:s');
            $stateParameters->data['updated_at'] = date('Y-m-d H:i:s');
            return $stateParameters;
        });

        $crud->setActionButton('Detail', 'fa fa-eye', function ($row) {
            //arahkan untuk menampilkan detail modal
            return "javascript:showModal('" . $row->id . "');";
        });
        $crud->callbackColumn('is_active', function ($value, $row) {
            if ($value == 0) {
                return '<span class="badge badge-success">Tersedia</span>';
            } else {
                return '<span class="badge badge-danger">Kosong</span>';
            }
        });
        $crud->callbackColumn('price', function ($value, $row) {
            return "Rp. " . number_format($value, 0, ",", ".");
        });

        $crud->displayAs($display);

        $crud->unsetJquery();
        $output = $crud->render();

        $this->_setOutput($output, 'index');
    }

    public function getModal($param)
    {
        $value = $this->M_produk->getDetail($param);
        if ($value["is_active"] == "0") {
            $status = '<span class="badge badge-success">Tersedia</span>';
        } else {
            $status = '<span class="badge badge-danger">Kosong</span>';
        }


        $modalContent = '
        <table class="table table-bordered" width="100%">
            <tr>
                <th>Nama</th>
                <th>Stok</th>
                <th>Status</th>
                <th>Kategori</th>
                <th>Warna</th>
                <th>Harga</th>
            </tr>
            <tr>
                <td>' . $value["name"] . '</td>
                <td>' . $value["stock"] . '</td>
                <td>' . $status . '</td>
                <td>' . $value["katNama"] . '</td>
                <td>' . $value["color"] . '</td>
                <td>Rp. ' . number_format($value["price"], 0, ",", ".") . '</td>
            </tr>
        </table>
        ';
        echo ($modalContent);
    }


    function _setOutput($output = null, $view = null)
    {
        if (isset($output->isJSONResponse) && $output->isJSONResponse) {
            header('Content-Type: application/json; charset=utf-8');
            echo $output->output;
            exit;
        }
        $x = array_merge($this->data, ['output' => $output]);
        $this->layout->set_template('template/app');
        $this->layout->CONTENT->view('produk/index/' . $view, $x);
        $this->layout->publish();
    }

    // public function index()
    // {
    //     $this->data['nama_menu'] = 'Data Produk';
    //     $this->data['condition'] = '<div class="breadcrumb-item active">Produk</div>';

    //     //load model tampil data
    //     $this->load->model('M_Produk');
    //     $this->data['produk'] = $this->M_Produk->getData()->result();

    //     $this->layout->set_template('template/app');
    //     $this->layout->CONTENT->view('produk/index/index', $this->data);
    //     $this->layout->publish();
    // }
}
